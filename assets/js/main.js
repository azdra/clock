let text = document.querySelector(".time");

const setDate = () => {
    let date = new Date();
    
    let years = date.getFullYear();
    let months = date.getMonth();
    let days = date.getDay();
    let hours = date.getHours() < 10 ? "0"+date.getHours() : date.getHours();
    let minutes = date.getMinutes() < 10 ? "0"+date.getMinutes() : date.getMinutes();
    let seconds = date.getSeconds() < 10 ? "0"+date.getSeconds() : date.getSeconds(); 
    
    text.innerHTML = `${hours}:${minutes}:${seconds}`;
}

setInterval(setDate);